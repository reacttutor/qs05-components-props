import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';

function Welcome(props: {[key: string]: any}) {  // ??
  return <h1>Hello, {props.name}</h1>;
}

function App() {
  return (
    <div>
      <Welcome name="Julie" />
      <Welcome name="Sally" />
      <Welcome name="Mary" />
    </div>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);

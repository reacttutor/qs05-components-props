# Components and Props

> [Quick Start: Components and Props](https://reactjs.org/docs/components-and-props.html)



    create-react-app components-props --scripts-version=react-scripts-ts
    cd components-props


Remove all "App" files under `/src` and update the `index.tsx`:

	function Welcome(props) {
	  return <h1>Hello, {props.name}</h1>;
	}

	function App() {
	  return (
		<div>
		  <Welcome name="Sara" />
		  <Welcome name="Cahal" />
		  <Welcome name="Edite" />
		</div>
	  );
	}

	ReactDOM.render(
	  <App />,
	  document.getElementById('root')
	);


Then, run a dev server:

    npm start




